from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Inscription
from .constances import forms
from .constances import constances  as c
import json


@csrf_exempt
def createInscription(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'POST':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.InitInscription(payload)
        if c.verifyToken(token=token) and form.is_valid():
            image = forms.cleaned_data["image"]
            id_atelier = forms.cleaned_data["id_atelier"]
            id_event = forms.cleaned_data["id_event"]
            id_user = forms.cleaned_data["id_user"]
            role = forms.cleaned_data["role"]
            try:
                des = Inscription.create(
                    image=image,
                    id_atelier=id_atelier,
                    id_event=id_event,
                    id_user=id_user,
                    role = role,
                )
                data["error"] = False
                data["code"] = 0
                data["data"] = des
                status = 200
            except Exception as e :
                data["error"] = True
                data["code"] = -1
                data['description'] = 'Database Writing error occured :' + e
                status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
        data['description'] = 'Unauthorized method GET'
    return JsonResponse(data, status=status)

@csrf_exempt
def updateInscription(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'POST':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.setInscription(payload)
        if c.verifyToken(token=token) and form.is_valid():
            uuid = form.cleaned_data["uuid"]
            image = forms.cleaned_data["image"]
            id_atelier = forms.cleaned_data["id_atelier"]
            id_event = forms.cleaned_data["id_event"]
            id_user = forms.cleaned_data["id_event"]
            role = forms.cleaned_data["role"]
            statut = forms.cleaned_data["statut"]
            try:
                des = Inscription.update(
                    uuid=uuid,
                    image=image,
                    id_atelier=id_atelier,
                    id_event=id_event,
                    id_user=id_user,
                    role = role,
                    statut = statut,
                )
                data["error"] = False
                data["code"] = 0
                data["data"] = des
                status = 200
            except Exception as e :
                data["error"] = True
                data["code"] = -1
                data['description'] = 'Database Writing error occured :' + e
                status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
        data['description'] = 'Unauthorized method'
    return JsonResponse(data, status=status)

@csrf_exempt
def getInscriptions(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'GET':
        token=request.META['HTTP_AUTHORIZATION']
        if c.verifyToken(token=token):
            des = Inscription.readAll()
            data["error"] = False
            data["code"] = 0
            data["data"] = des
            status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
    return JsonResponse(data, status=status)

@csrf_exempt
def getInscription(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'GET':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.getInscription(payload)
        if c.verifyToken(token=token) and form.is_valid() :
            des = Inscription.read(
                uuid=form.cleaned_data["uuid"]
            )
            data["error"] = False
            data["code"] = 0
            data["data"] = des
            status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
    return JsonResponse(data, status=status)


