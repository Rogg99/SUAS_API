from django import forms
from . import constances as c 

class InitEvent(forms.Form):
    id_user = forms.CharField(required=True)
    id_categorie = forms.CharField(required=True)
    libelle = forms.CharField(required=True)
    logo = forms.CharField(required=True)
    lieu = forms.CharField(required=True)
    description = forms.CharField(required=True)                                                              
    date_debut = forms.DateTimeField(required=True)
    date_fin = forms.DateTimeField(required=True)

    def __init__(self, *args, **kwargs):
        super(InitEvent, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setEvent(forms.Form):
    uuid = forms.CharField(required=True)
    id_user = forms.CharField(required=True)
    id_categorie = forms.CharField(required=True)
    libelle = forms.CharField(required=True)
    logo = forms.CharField(required=True)
    statut = forms.CharField(required=True)
    lieu = forms.CharField(required=True)
    description = forms.CharField(required=True)                                                              
    date_debut = forms.DateTimeField(required=True)
    date_fin = forms.DateTimeField(required=True)

    def __init__(self, *args, **kwargs):
        super(setEvent, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getEvent(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getEvent, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data


class InitAtelier(forms.Form):
    id_event = forms.CharField(required=True)
    categorie_atelier = forms.CharField(required=True)
    id_user = forms.CharField(required=True)
    id_categorie = forms.CharField(required=True)
    libelle = forms.CharField(required=True)
    nb_placep = forms.CharField(required=True)
    nb_placev = forms.CharField(required=True)
    meeting_url = forms.CharField(required=True)
    price = forms.CharField(required=True)
    logo = forms.CharField(required=True)
    salle = forms.CharField(required=True)
    description = forms.CharField(required=True)                                                              
    date_debut = forms.DateTimeField(required=True)
    date_fin = forms.DateTimeField(required=True)

    def __init__(self, *args, **kwargs):
        super(InitAtelier, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getAtelier(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getAtelier, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setAtelier(forms.Form):
    uuid = forms.CharField(required=True)
    id_event = forms.CharField(required=True)
    categorie_atelier = forms.CharField(required=True)
    id_user = forms.CharField(required=True)
    id_categorie = forms.CharField(required=True)
    libelle = forms.CharField(required=True)
    nb_placep = forms.CharField(required=True)
    nb_placev = forms.CharField(required=True)
    meeting_url = forms.CharField(required=True)
    price = forms.CharField(required=True)
    logo = forms.CharField(required=True)
    salle = forms.CharField(required=True)
    description = forms.CharField(required=True)                                                              
    date_debut = forms.DateTimeField(required=True)
    date_fin = forms.DateTimeField(required=True)
    statut = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getAtelier, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data


class InitNotification(forms.Form):
    id_event = forms.CharField(required=True)
    id_atelier = forms.CharField(required=True)
    content = forms.CharField(required=True)
    title = forms.CharField(required=True)
    type = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(InitNotification, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setNotification(forms.Form):
    uuid = forms.CharField(required=True)
    id_event = forms.CharField(required=True)
    id_atelier = forms.CharField(required=True)
    content = forms.CharField(required=True)
    title = forms.CharField(required=True)
    type = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(setNotification, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getNotification(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getNotification, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data


class InitMessage(forms.Form):
    id_user = forms.CharField(required=True)
    id_categorie = forms.CharField(required=True)
    libelle = forms.CharField(required=True)
    logo = forms.CharField(required=True)
    lieu = forms.CharField(required=True)
    description = forms.CharField(required=True)                                                              
    date_debut = forms.DateTimeField(required=True)
    date_fin = forms.DateTimeField(required=True)

    def __init__(self, *args, **kwargs):
        super(InitMessage, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setMessage(forms.Form):
    uuid = forms.CharField(required=True)
    id_user = forms.CharField(required=True)
    id_categorie = forms.CharField(required=True)
    libelle = forms.CharField(required=True)
    logo = forms.CharField(required=True)
    lieu = forms.CharField(required=True)
    description = forms.CharField(required=True)                                                              
    date_debut = forms.DateTimeField(required=True)
    date_fin = forms.DateTimeField(required=True)
    statut = forms.DateTimeField(required=True)

    def __init__(self, *args, **kwargs):
        super(setMessage, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getMessage(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getMessage, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data


class InitDiscussion(forms.Form):
    id_user = forms.CharField(required=True)
    id_categorie = forms.CharField(required=True)
    libelle = forms.CharField(required=True)
    logo = forms.CharField(required=True)
    lieu = forms.CharField(required=True)
    description = forms.CharField(required=True)                                                              
    date_debut = forms.DateTimeField(required=True)
    date_fin = forms.DateTimeField(required=True)

    def __init__(self, *args, **kwargs):
        super(InitDiscussion, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setDiscussion(forms.Form):
    uuid = forms.CharField(required=True)
    id_user = forms.CharField(required=True)
    id_categorie = forms.CharField(required=True)
    libelle = forms.CharField(required=True)
    logo = forms.CharField(required=True)
    lieu = forms.CharField(required=True)
    description = forms.CharField(required=True)                                                              
    date_debut = forms.DateTimeField(required=True)
    date_fin = forms.DateTimeField(required=True)
    statut = forms.DateTimeField(required=True)

    def __init__(self, *args, **kwargs):
        super(setDiscussion, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getDiscussion(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getDiscussion, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data


class InitInsight(forms.Form):
    id_event = forms.CharField(required=True)
    lienUrl = forms.CharField(required=True)
    image = forms.CharField(required=True)
    titre = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(InitInsight, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setInsight(forms.Form):
    uuid = forms.CharField(required=True)
    id_event = forms.CharField(required=True)
    lienUrl = forms.CharField(required=True)
    image = forms.CharField(required=True)
    titre = forms.CharField(required=True)
    statut = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(setInsight, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getInsight(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getInsight, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data


class InitGallerieImage(forms.Form):
    id_event = forms.CharField(required=True)
    id_atelier = forms.CharField(required=True)
    image = forms.CharField(required=True)
    comment = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(InitGallerieImage, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setGallerieImage(forms.Form):
    uuid = forms.CharField(required=True)
    id_event = forms.CharField(required=True)
    id_atelier = forms.CharField(required=True)
    image = forms.CharField(required=True)
    comment = forms.CharField(required=True)
    statut = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(setGallerieImage, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getGallerieImage(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getGallerieImage, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data


class InitPartenaire(forms.Form):
    id_event = forms.CharField(required=True)
    logo = forms.CharField(required=True)
    siteweb = forms.CharField(required=True)
    custumerMail = forms.CharField(required=True)
    addressPostale = forms.CharField(required=True)
    addressTelephone = forms.CharField(required=True)
    addressLieu = forms.CharField(required=True)
    type_partenaire = forms.CharField(required=True)
    presentation = forms.CharField(required=True)   

    def __init__(self, *args, **kwargs):
        super(InitPartenaire, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setPartenaire(forms.Form):
    uuid = forms.CharField(required=True)
    id_event = forms.CharField(required=True)
    logo = forms.CharField(required=True)
    siteweb = forms.CharField(required=True)
    custumerMail = forms.CharField(required=True)
    addressPostale = forms.CharField(required=True)
    addressTelephone = forms.CharField(required=True)
    addressLieu = forms.CharField(required=True)
    type_partenaire = forms.CharField(required=True)
    presentation = forms.CharField(required=True)  
    statut = forms.CharField(required=True)   

    def __init__(self, *args, **kwargs):
        super(setPartenaire, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getPartenaire(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getPartenaire, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data


class InitIntervenant(forms.Form):
    id_event = forms.CharField(required=True)
    id_atelier = forms.CharField(required=True)
    image = forms.CharField(required=True)
    type_intervenant = forms.CharField(required=True)
    presentation = forms.CharField(required=True) 

    def __init__(self, *args, **kwargs):
        super(InitIntervenant, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setIntervenant(forms.Form):
    uuid = forms.CharField(required=True)
    id_event = forms.CharField(required=True)
    id_atelier = forms.CharField(required=True)
    image = forms.CharField(required=True)
    type_intervenant = forms.CharField(required=True)
    presentation = forms.CharField(required=True) 
    statut = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(setIntervenant, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getIntervenant(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getIntervenant, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data


class InitInscription(forms.Form):
    id_user = forms.CharField(required=True)
    id_event = forms.CharField(required=True)
    id_atelier = forms.CharField(required=True)
    role = forms.CharField(required=True)
    presentation = forms.CharField(required=True) 

    def __init__(self, *args, **kwargs):
        super(InitInscription, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setInscription(forms.Form):
    uuid = forms.CharField(required=True)
    id_user = forms.CharField(required=True)
    id_event = forms.CharField(required=True)
    id_atelier = forms.CharField(required=True)
    role = forms.CharField(required=True)
    presentation = forms.CharField(required=True) 
    statut = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(setInscription, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getInscription(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getInscription, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data


class InitVideo(forms.Form):
    id_event = forms.CharField(required=True)
    id_atelier = forms.CharField(required=True)
    url = forms.CharField(required=True)
    comment = forms.CharField(required=True)  

    def __init__(self, *args, **kwargs):
        super(InitVideo, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class setVideo(forms.Form):
    uuid = forms.CharField(required=True)
    id_event = forms.CharField(required=True)
    id_atelier = forms.CharField(required=True)
    url = forms.CharField(required=True)
    comment = forms.CharField(required=True) 
    statut = forms.CharField(required=True)  

    def __init__(self, *args, **kwargs):
        super(setVideo, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data

class getVideo(forms.Form):
    uuid = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(getVideo, self).__init__(*args, **kwargs)

    def clean(self):
        return self.cleaned_data




