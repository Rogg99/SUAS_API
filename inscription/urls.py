from django.urls import path
from . import views

urlpatterns = [
    path("Create", views.createInscription, name="createInscription"),
    path("Update", views.updateInscription, name="updateInscription"),
    path("Get", views.getInscription, name="getInscription"),
    path("GetAll", views.getInscriptions, name="getInscriptions"),
]