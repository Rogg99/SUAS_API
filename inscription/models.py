from django.db import models
from .constances import constances as c
import datetime, uuid

# Create your models here.
class Inscription(models.Model):
    uuid = models.CharField(max_length=100,primary_key=True)
    id_user = models.CharField(max_length=100,null=False)
    id_event = models.CharField(max_length=100,null=False)
    id_atelier = models.CharField(max_length=100,null=False)
    role = models.CharField(max_length=100, choices=c.INTERVENANT_CHOICE,default='1')
    date_init = models.DateField(default=datetime.datetime.now())
    statut = models.CharField(max_length=100,null=False,choices=c.STATUT_VALIDATION_CHOICE)
    
    class Meta:
        ordering =["date_init"]

    def __str__(self):
        return self.uuid

    def create(id_user: str, id_event: str, id_atelier: str, role: str):
        da = Inscription()
        codefin = datetime.datetime.now().strftime("%Y-%m-%d")
        da.uuid = str(uuid.uuid4()) + ":"+str(len(Inscription.objects.filter(date_init=codefin)))
        da.id_atelier = id_atelier
        da.id_user = id_user
        da.id_event = id_event
        da.role = role
        da.statut = '1'
        da.save()
        return {
                    "uuid":da.uuid,
                    "id_user": da.id_user,
                    "id_atelier":da.id_atelier,
                    "role":da.role,
                    "statut":da.statut,
                    "date_init" : da.date_init,
                    }
    create = staticmethod(create)

    def update(uuid: str,id_user: str, id_event: str, id_atelier: str, role: str, statut: str):
        da = Inscription.objects.get(uuid=uuid)
        da.id_atelier = id_atelier
        da.id_user = id_user
        da.id_event = id_event
        da.role = role
        da.statut = statut
        da.save()
        return {
                    "uuid":da.uuid,
                    "id_user": da.id_user,
                    "id_event": da.id_event,
                    "id_atelier":da.id_atelier,
                    "role":da.role,
                    "statut":da.statut,
                    "date_init" : da.date_init,
                    }
    update = staticmethod(update)

    def read(uuid:str):
         item = Inscription.objects.get(uuid=uuid)
         return {
                    "uuid":item.uuid,
                    "id_user": item.id_user,
                    "id_atelier":item.id_atelier,
                    "role":item.role,
                    "statut":item.statut,
                    "date_init" : item.date_init,
                    }
    read = staticmethod(read)

    def readAll(id_user:str):
         events = Inscription.objects.filter(id_user=id_user)
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_user": item.id_user,
                    "id_atelier":item.id_atelier,
                    "role":item.role,
                    "statut":item.statut,
                    "date_init" : item.date_init,
                    })
         return res
    readAll = staticmethod(readAll)
