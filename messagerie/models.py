from django.db import models
from .constances import constances as c
import datetime, uuid

# Create your models here.
class Message(models.Model):
    uuid = models.CharField(max_length=100,primary_key=True,unique=True)
    id_sender = models.CharField(max_length=100,null=False)
    id_discussion = models.CharField(max_length=100,null=False)
    content = models.CharField(max_length=100, null=False)
    state = models.CharField(max_length=100, null=False,choices=c.MESSAGE_STATE,default='1')
    date_init = models.DateField(auto_now_add=True)

    class Meta:
        ordering =["date_init"]

    def __str__(self):
        return self.uuid

    def create(id_sender: str, state: str, id_discussion: str, content: str):
        da = Message()
        codefin = datetime.datetime.now().strftime("%m/%Y")
        da.uuid = str(uuid.uuid4()) + ":"+str(len(Message.objects.filter(date_init=codefin)))
        da.id_sender = id_sender
        da.id_discussion = id_discussion
        da.content = content
        da.state = state
        da.save()
        return da
    create = staticmethod(create)

    def read(uuid:str):
         events = Message.objects.filter(uuid=uuid)
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_sender": item.id_sender,
                    "state":item.state,
                    "id_discussion":item.id_discussion,
                    "content" : item.content,   
                    "date_init" : item.date_init,
                    })
         return res
    read = staticmethod(read)


# Create your models here.
class Discussion(models.Model):
    uuid = models.CharField(max_length=100,primary_key=True,unique=True)
    id_promoteur = models.CharField(max_length=100,null=False)
    id_client = models.CharField(max_length=100,null=False)
    id_last_message = models.CharField(max_length=100, null=False)
    state = models.CharField(max_length=100, null=False,choices=c.MESSAGE_STATE,default='1')
    date_init = models.DateField(auto_now_add=True)

    class Meta:
        ordering =["date_init"]

    def __str__(self):
        return self.uuid

    def create(id_promoteur: str, state: str, id_client: str, id_last_message: str):
        da = Discussion()
        codefin = datetime.datetime.now().strftime("%m/%Y")
        da.uuid = str(uuid.uuid4()) + ":"+str(len(Discussion.objects.filter(date_init=codefin)))
        da.id_promoteur = id_promoteur
        da.id_client = id_client
        da.id_last_message = id_last_message
        da.state = state
        da.save()
        return da
    create = staticmethod(create)

    def read(uuid:str):
         events = Discussion.objects.filter(uuid=uuid)
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_promoteur": item.id_promoteur,
                    "state":item.state,
                    "id_client":item.id_client,
                    "id_last_message" : item.id_last_message,   
                    "date_init" : item.date_init,
                    })
         return res
    read = staticmethod(read)
