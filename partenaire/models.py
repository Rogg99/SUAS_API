from django.db import models
from .constances import constances as c
import datetime, uuid

# Create your models here.
class Partenaire(models.Model):
    uuid = models.CharField(max_length=100,primary_key=True,unique=True)
    id_event = models.CharField(max_length=100,null=False)
    logo = models.CharField(max_length=10000000,null=False)
    siteweb = models.CharField(max_length=200,null=False)
    custumerMail = models.CharField(max_length=200,null=False)
    addressPostale = models.CharField(max_length=200,null=False)
    addressTelephone = models.CharField(max_length=200,null=False)
    addressLieu = models.CharField(max_length=200,null=False)
    type_partenaire = models.CharField(max_length=100, null=False,choices=c.PARTENAIRE_CHOICE)
    presentation = models.CharField(max_length=2000, null=False, blank=True, default='')                                                              
    date_init = models.DateField(auto_now_add=True)
    statut = models.CharField(max_length=100,null=False,choices=c.STATUT_VALIDATION_CHOICE)
    
    class Meta:
        ordering =["date_init"]

    def __str__(self):
        return self.uuid

    def create(id_event: str, logo: str, siteweb: str, addressLieu: str, custumerMail: str, addressPostale: str, addressTelephone: str, type_partenaire: str, presentation: str):
        da = Partenaire()
        codefin = datetime.datetime.now().strftime("%m/%Y")
        da.uuid = str(uuid.uuid4()) + ":"+str(len(Partenaire.objects.filter(date_init=codefin)))
        da.logo = logo
        da.siteweb = siteweb
        da.custumerMail = custumerMail
        da.addressPostale = addressPostale
        da.addressTelephone = addressTelephone
        da.addressLieu = addressLieu
        da.id_event = id_event
        da.type_partenaire = type_partenaire
        da.presentation = presentation
        da.statut = '1'
        da.save()
        return da
    create = staticmethod(create)

    def update(uuid:str,id_event: str, statut: str, logo: str, siteweb: str, addressLieu: str, custumerMail: str, addressPostale: str, addressTelephone: str, type_partenaire: str, presentation: str):
        da = Partenaire.objects.get(uuid=uuid)
        da.logo = logo
        da.siteweb = siteweb
        da.custumerMail = custumerMail
        da.addressPostale = addressPostale
        da.addressTelephone = addressTelephone
        da.addressLieu = addressLieu
        da.id_event = id_event
        da.type_partenaire = type_partenaire
        da.presentation = presentation
        da.statut = statut
        da.save()
        return da
    update = staticmethod(update)

    def read(uuid:str):
         events = Partenaire.objects.filter(uuid=uuid)
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "siteweb":item.siteweb,
                    "custumerMail":item.custumerMail,
                    "addressPostale":item.addressPostale,
                    "addressTelephone":item.addressTelephone,
                    "addressTelephone":item.addressTelephone,
                    "addressLieu":item.addressLieu,
                    "logo":item.logo,
                    "statut":item.statut,
                    "presentation" : item.presentation,   
                    "date_init" : item.date_init,
                    })
         return res
    read = staticmethod(read)

    def readAll(id_event:str):
        events = Partenaire.objects.filter(id_event=id_event).exclude(statut=c.STATUT_VALIDATION_CHOICE[4])
        res = []
        for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "siteweb":item.siteweb,
                    "custumerMail":item.custumerMail,
                    "addressPostale":item.addressPostale,
                    "addressTelephone":item.addressTelephone,
                    "addressTelephone":item.addressTelephone,
                    "addressLieu":item.addressLieu,
                    "logo":item.logo,
                    "statut":item.statut,
                    "presentation" : item.presentation,   
                    "date_init" : item.date_init,
                    })
        return res
    readAll = staticmethod(readAll)
