import requests
import json
User_END_POINT = "https://bfc.api.user.zukulufeg.com/profil/myprofil"
#END_POINT = "http://127.0.0.1:8080"
DE_END_POINT = "http://127.0.0.1:8080"
STATUT_VALIDATION_CHOICE = [
    ('1', 'waiting_for_response'),
    ('2', 'acccepted'),
    ('3', 'rejected'),
    ('4', 'deleted'),
    ('5', 'suspended'),
]

CATEGORIE_CHOICE = [
    ('1', 'GLOBAL BUSINESS'),
    ('2', 'B2B/B2C '),
    ('3', 'FORMATION ET COACHING'),
    ('4', 'REFLEXIONS'),
    ('5', 'COCKTAILS ET NETWORKING'),
    ('6', 'B.LEAD HER'),
    ('7', 'G100'),
    ('8', 'CREATIF'),
]

NOTIFICATION_TYPE = [
    ('1', 'UPDATE'),
    ('2', 'ADD'),
    ('3', 'REMIND'),
]

MESSAGE_STATE = [
    ('1', 'UNREAD'),
    ('2', 'READ'),
    ('3', 'DELETED'),
    ('4', 'WAITING'),
]

EVENT_CATEGORIE_CHOICE = [
    ('1', 'CONGRES'),
    ('2', 'SEMINAIRE'),
    ('3', 'SALON PROFESSIONEL'),
    ('4', 'TEAM BUILDING'),
    ('5', 'INCENTIVE'),
    ('6', 'CONFERENCE'),
    ('7', 'LANCEMENT DE PRODUIT'),
]

INTERVENANT_CHOICE = [
    ('1', 'Participant'),
    ('2', 'Paneliste'),
    ('3', 'Expert'),
]

PARTENAIRE_CHOICE = [
    ('1', 'PARTENAIRE'),
    ('2', 'CO-ORGANISATEUR'),
]

def verifyToken(token: str):
    url = User_END_POINT
    res = requests.get(url=url,headers={"Authorization": token})
    if(res.status_code!=200):
        return False
    return True

