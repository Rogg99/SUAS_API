from django.urls import path
from . import views

urlpatterns = [
    path("Create", views.createEvent, name="createEvent"),
    path("Update", views.updateEvent, name="updateEvent"),
    path("Get", views.getEvent, name="getEvent"),
    path("GetAll", views.getEvents, name="getEvents"),
]