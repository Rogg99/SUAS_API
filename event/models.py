from django.db import models
from .constances import constances as c
import datetime, uuid


# Create your models here.
# Create your models here.
class Evenement(models.Model):
    uuid = models.CharField(max_length=100)
    id_user = models.CharField(max_length=100)
    id_categorie = models.CharField(max_length=100)
    libelle = models.CharField(max_length=70)
    logo = models.CharField(max_length=10000000)
    lieu = models.CharField(max_length=100)
    description = models.CharField(max_length=1000, null=False, blank=True, default='')                                                              
    date_debut = models.DateTimeField(null=False, blank=False)
    date_fin = models.DateTimeField(null=False, blank=False)
    date_init = models.DateField(default=datetime.datetime.now())
    statut = models.CharField(max_length=100, choices=c.EVENT_CATEGORIE_CHOICE)
    active = models.BooleanField(default=False)
    
    class Meta:
        ordering =["date_init"]

    def __str__(self):
        return self.uuid

    def create(id_user: str, id_categorie: str, date_debut: str, date_fin: str, description: str, lieu:str, logo:str,libelle:str):
        da = Evenement()
        #print(da.date_init)
        codefin = datetime.datetime.now().strftime("%Y-%m-%d")
        da.uuid = str(uuid.uuid4()) + ":"+str(len(Evenement.objects.filter(date_init=codefin)))
        da.id_categorie = id_categorie
        da.libelle = libelle
        da.id_user = id_user
        da.logo = logo
        da.date_fin = datetime.datetime.strftime(date_fin,'%Y-%m-%d')
        da.date_debut = datetime.datetime.strftime(date_debut,'%Y-%m-%d')
        da.description = description
        da.lieu = lieu
        #da.init=datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d')
        da.statut = '1'
        da.save()
        return {
                    "uuid":da.uuid,
                    "id_user": da.id_user,
                    "id_categorie":da.id_categorie,
                    "libelle":da.libelle,
                    "lieu":da.lieu,
                    "logo":da.logo,
                    "active":da.active,
                    "statut":da.statut,
                    "description" : da.description,                                                        
                    "date_debut" : da.date_debut,
                    "date_fin" : da.date_fin,
                    "date_init" : da.date_init,
                    }
    create = staticmethod(create)

    def update(uuid: str,id_user: str, active: str, id_categorie: str, statut: str, date_debut: str, date_fin: str, description: str, lieu:str, logo:str,libelle:str):
        da = Evenement.objects.get(uuid=uuid)
        da.id_categorie = id_categorie
        da.libelle = libelle
        da.id_user = id_user
        da.logo = logo
        da.date_fin = date_fin
        da.date_debut = date_debut
        da.description = description
        da.lieu = lieu
        da.statut = statut
        da.active = active
        da.save()
        return {
                    "uuid":da.uuid,
                    "id_user": da.id_user,
                    "id_categorie":da.id_categorie,
                    "libelle":da.libelle,
                    "lieu":da.lieu,
                    "logo":da.logo,
                    "active":da.active,
                    "statut":da.statut,
                    "description" : da.description,                                                        
                    "date_debut" : da.date_debut,
                    "date_fin" : da.date_fin,
                    "date_init" : da.date_init,
                    }
    update = staticmethod(update)

    def read(uuid:str):
         res = Evenement.objects.get(uuid=uuid)
         return {
                    "uuid":res.uuid,
                    "id_user": res.id_user,
                    "id_categorie":res.id_categorie,
                    "libelle":res.libelle,
                    "lieu":res.lieu,
                    "logo":res.logo,
                    "active":res.active,
                    "statut":res.statut,
                    "description" : res.description,                                                        
                    "date_debut" : res.date_debut,
                    "date_fin" : res.date_fin,
                    "date_init" : res.date_init,
                    }
    read = staticmethod(read)

    def readAll():
         events = Evenement.objects.exclude(statut = '5')
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_user": item.id_user,
                    "id_categorie":item.id_categorie,
                    "libelle":item.libelle,
                    "lieu":item.lieu,
                    "logo":item.logo,
                    "active":item.active,
                    "statut":item.statut,
                    "description" : item.description,                                                        
                    "date_debut" : item.date_debut,
                    "date_fin" : item.date_fin,
                    "date_init" : item.date_init,
                    })
         return res
    readAll = staticmethod(readAll)
