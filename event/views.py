from django.shortcuts import render
from django.http import JsonResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from .models import Evenement
from .constances import forms
from .constances import constances  as c
import json


@csrf_exempt
def createEvent(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'POST':
        token=request.META['HTTP_AUTHORIZATION']
        #print('*********** request token :',token)
        payload = json.loads(request.body)
        form = forms.InitEvent(payload)
        if c.verifyToken(token=token) and form.is_valid():
            id_user = form.cleaned_data["id_user"]
            id_categorie = form.cleaned_data["id_categorie"]
            libelle = form.cleaned_data["libelle"]
            logo = form.cleaned_data["logo"]
            lieu = form.cleaned_data["lieu"]
            description = form.cleaned_data["description"]
            date_debut = form.cleaned_data["date_debut"]
            date_fin = form.cleaned_data["date_fin"]
            try:
                des = Evenement.create(
                    id_user=id_user,
                    id_categorie=id_categorie,
                    libelle=libelle,
                    logo=logo,
                    lieu=lieu,
                    description=description,
                    date_debut=date_debut,
                    date_fin=date_fin
                )
                data["error"] = False
                data["code"] = 0
                data["data"] = {
                    "id":des.uuid,
                    "libelle":des.libelle,
                    "date_init":des.date_init,
                    "statut":des.statut
                }

            except Exception as e :
                data["error"] = True
                data["code"] = -1
                data['description'] = 'Database Writing error occured :'+str(e)
                status = 200
        elif not form.is_valid():
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = form.errors.values
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
        data['description'] = 'Unauthorized method '+request.method
    return JsonResponse(data, status=status)

@csrf_exempt
def updateEvent(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'POST':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.setEvent(payload)
        if c.verifyToken(token=token) and form.is_valid():
            uuid = form.cleaned_data["uuid"]
            id_user = form.cleaned_data["id_user"]
            id_categorie = form.cleaned_data["id_categorie"]
            libelle = form.cleaned_data["libelle"]
            statut = form.cleaned_data["statut"]
            logo = form.cleaned_data["logo"]
            lieu = form.cleaned_data["lieu"]
            description = form.cleaned_data["description"]
            date_debut = form.cleaned_data["date_debut"]
            date_fin = form.cleaned_data["date_fin"]
            try:
                des = Evenement.update(
                    uuid=uuid,
                    id_user=id_user,
                    id_categorie=id_categorie,
                    libelle=libelle,
                    statut=statut,
                    logo=logo,
                    lieu=lieu,
                    description=description,
                    date_debut=date_debut,
                    date_fin=date_fin
                )
                data["error"] = False
                data["code"] = 0
                data["data"] = [
                    {
                        "uuid": des.uuid,
                        "libelle":des.libelle,
                        "statut":des.statut,
                                 }]
                status = 200
            except Exception as e :
                data["error"] = True
                data["code"] = -1
                data['description'] = 'Database Writing error occured :' + e
                status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
        data['description'] = 'Unauthorized method '+request.method
    return JsonResponse(data, status=status)

@csrf_exempt
def getEvents(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'GET':
        token=request.META['HTTP_AUTHORIZATION']
        if c.verifyToken(token=token):
            des = Evenement.readAll()
            data["error"] = False
            data["code"] = 0
            data["data"] = des
            status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
    return JsonResponse(data, status=status)

@csrf_exempt
def getEvent(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'GET':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.getEvent(payload)
        if c.verifyToken(token=token) and form.is_valid() :
            des = Evenement.read(
                uuid=form.cleaned_data["uuid"]
            )
            data["error"] = False
            data["code"] = 0
            data["data"] = des
            status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
    return JsonResponse(data, status=status)


