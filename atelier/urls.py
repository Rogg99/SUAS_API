from django.urls import path
from . import views

urlpatterns = [
    path("Create", views.createAtelier, name="createAtelier"),
    path("Update", views.updateAtelier, name="updateAtelier"),
    path("Get", views.getAtelier, name="getAtelier"),
    path("GetAll", views.getAteliers, name="getAteliers"),
]