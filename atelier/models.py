from django.db import models
from .constances import constances as c
import datetime, uuid

# Create your models here.
class Atelier(models.Model):
    uuid = models.CharField(max_length=100)
    id_event = models.CharField(max_length=100)
    categorie_atelier = models.CharField(max_length=100, choices=c.CATEGORIE_CHOICE)
    libelle = models.CharField(max_length=70)
    logo = models.CharField(max_length=10000000)
    salle = models.CharField(max_length=100)
    meeting_url = models.CharField(max_length=200)
    nb_placep = models.CharField(max_length=200)
    nb_placev = models.CharField(max_length=200)
    price = models.CharField(max_length=200)
    meeting_url = models.CharField(max_length=200)
    description = models.CharField(max_length=1000, null=False, blank=True, default='')                                                              
    date_debut = models.DateTimeField(null=False, blank=False)
    date_fin = models.DateTimeField(null=False, blank=False)
    date_init = models.DateField(default=datetime.datetime.now())
    statut = models.CharField(max_length=100, choices=c.STATUT_VALIDATION_CHOICE)
    active = models.BooleanField(default=False)
    
    class Meta:
        ordering =["date_init"]

    def __str__(self):
        return self.uuid

    def create(id_event: str, categorie_atelier: str, date_debut: str, nb_placep: str, nb_placev: str, price: str, date_fin: str, meeting_url: str, description: str, salle:str, logo:str,libelle:str):
        da = Atelier()
        codefin = datetime.datetime.now().strftime("%Y-%m-%d")
        da.uuid = str(uuid.uuid4()) + ":"+str(len(Atelier.objects.filter(date_init=codefin)))
        da.categorie_atelier = categorie_atelier
        da.libelle = libelle
        da.id_event = id_event
        da.logo = logo
        da.nb_placep = nb_placep
        da.nb_placev = nb_placev
        da.meeting_url = meeting_url
        da.price = price
        da.date_fin = datetime.datetime.strftime(date_fin,'%Y-%m-%d')
        da.date_debut = datetime.datetime.strftime(date_debut,'%Y-%m-%d')
        da.description = description
        da.salle = salle
        da.statut = '1'
        da.save() 
        return {
                    "uuid":da.uuid,
                    "id_event": da.id_event,
                    "categorie_atelier":da.categorie_atelier,
                    "libelle":da.libelle,
                    "salle":da.salle,
                    "nb_placep":da.nb_placep,
                    "nb_placev":da.nb_placev,
                    "price":da.price,
                    "meeting_url":da.meeting_url,
                    "logo":da.logo,
                    "active":da.active,
                    "statut":da.statut,
                    "description" : da.description,                                                        
                    "date_debut" : da.date_debut,
                    "date_fin" : da.date_fin,
                    "date_init" : da.date_init,
                    }
    create = staticmethod(create)

    def update(uuid:str,id_event: str, categorie_atelier: str, statut: str, nb_placep: str, nb_placev: str, price: str, date_debut: str, date_fin: str, meeting_url: str, description: str, salle:str, logo:str,libelle:str):
        da = Atelier.objects.get(uuid=uuid)
        da.categorie_atelier = categorie_atelier
        da.libelle = libelle
        da.id_event = id_event
        da.logo = logo
        da.nb_placep = nb_placep
        da.nb_placev = nb_placev
        da.price = price
        da.date_fin = date_fin
        da.date_debut = date_debut
        da.meeting_url = meeting_url
        da.description = description
        da.salle = salle
        da.statut = statut
        da.save()
        return {
                    "uuid":da.uuid,
                    "id_event": da.id_event,
                    "categorie_atelier":da.categorie_atelier,
                    "libelle":da.libelle,
                    "salle":da.salle,
                    "nb_placep":da.nb_placep,
                    "nb_placev":da.nb_placev,
                    "price":da.price,
                    "meeting_url":da.meeting_url,
                    "logo":da.logo,
                    "active":da.active,
                    "statut":da.statut,
                    "description" : da.description,                                                        
                    "date_debut" : da.date_debut,
                    "date_fin" : da.date_fin,
                    "date_init" : da.date_init,
                    }
    update = staticmethod(update)

    def read(uuid:str):
         item = Atelier.objects.get(uuid=uuid)
         return {
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "categorie_atelier":item.categorie_atelier,
                    "libelle":item.libelle,
                    "salle":item.salle,
                    "nb_placep":item.nb_placep,
                    "nb_placev":item.nb_placev,
                    "price":item.price,
                    "meeting_url":item.meeting_url,
                    "logo":item.logo,
                    "active":item.active,
                    "statut":item.statut,
                    "description" : item.description,                                                        
                    "date_debut" : item.date_debut,
                    "date_fin" : item.date_fin,
                    "date_init" : item.date_init,
                    }
    read = staticmethod(read)

    def readAll():
         events = Atelier.objects.exclude(statut='4')
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "categorie_atelier":item.categorie_atelier,
                    "libelle":item.libelle,
                    "nb_placep":item.nb_placep,
                    "nb_placev":item.nb_placev,
                    "price":item.price,
                    "salle":item.salle,
                    "meeting_url":item.meeting_url,
                    "logo":item.logo,
                    "active":item.active,
                    "statut":item.statut,
                    "description" : item.description,                                                        
                    "date_debut" : item.date_debut,
                    "date_fin" : item.date_fin,
                    "date_init" : item.date_init,
                    })
         return res
    readAll = staticmethod(readAll)
