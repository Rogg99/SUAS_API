from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Atelier
from .constances import forms
from .constances import constances  as c
import json


@csrf_exempt
def createAtelier(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'POST':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.InitAtelier(payload)
        if c.verifyToken(token=token) and form.is_valid():
            id_event = form.cleaned_data["id_event"]
            categorie_atelier = form.cleaned_data["categorie_atelier"]
            id_user = form.cleaned_data["id_user"]
            libelle = form.cleaned_data["libelle"]
            nb_placep = form.cleaned_data["nb_placep"]
            nb_placev = form.cleaned_data["nb_placev"]
            meeting_url = form.cleaned_data["meeting_url"]
            price = form.cleaned_data["price"]
            salle = form.cleaned_data["salle"]
            statut = form.cleaned_data["statut"]
            logo = form.cleaned_data["logo"]
            description = form.cleaned_data["description"]                                                              
            date_debut = form.cleaned_data["date_debut"]
            date_fin = form.cleaned_data["date_fin"]
            try:
                des = Atelier.create(
                    id_user=id_user,
                    libelle=libelle,
                    logo=logo,
                    id_event = id_event,
                    categorie_atelier = categorie_atelier,
                    nb_placep = nb_placep,
                    nb_placev = nb_placev,
                    meeting_url = meeting_url,
                    price = price,
                    salle = salle,
                    statut = statut,
                    description=description,
                    date_debut=date_debut,
                    date_fin=date_fin
                )
                data["error"] = False
                data["code"] = 0
                data["data"] = des
                status = 200
            except Exception as e :
                data["error"] = True
                data["code"] = -1
                data['description'] = 'Database Writing error occured :' + e
                status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
        data['description'] = 'Unauthorized method GET'
    return JsonResponse(data, status=status)

@csrf_exempt
def updateAtelier(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'POST':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.setAtelier(payload)
        if c.verifyToken(token=token) and form.is_valid():
            uuid = form.cleaned_data["uuid"]
            id_event = form.cleaned_data["id_event"]
            categorie_atelier = form.cleaned_data["categorie_atelier"]
            libelle = form.cleaned_data["libelle"]
            nb_placep = form.cleaned_data["nb_placep"]
            nb_placev = form.cleaned_data["nb_placev"]
            meeting_url = form.cleaned_data["meeting_url"]
            price = form.cleaned_data["price"]
            salle = form.cleaned_data["salle"]
            statut = form.cleaned_data["statut"]
            logo = form.cleaned_data["logo"]
            description = form.cleaned_data["description"]                                                              
            date_debut = form.cleaned_data["date_debut"]
            date_fin = form.cleaned_data["date_fin"]
            try:
                des = Atelier.update(
                    uuid=uuid,
                    libelle=libelle,
                    logo=logo,
                    id_event = id_event,
                    categorie_atelier = categorie_atelier,
                    nb_placep = nb_placep,
                    nb_placev = nb_placev,
                    meeting_url = meeting_url,
                    price = price,
                    salle = salle,
                    statut = statut,
                    description=description,
                    date_debut=date_debut,
                    date_fin=date_fin
                )
                data["error"] = False
                data["code"] = 0
                data["data"] = des
                status = 200
            except Exception as e :
                data["error"] = True
                data["code"] = -1
                data['description'] = 'Database Writing error occured :' + e
                status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
        data['description'] = 'Unauthorized method'
    return JsonResponse(data, status=status)

@csrf_exempt
def getAteliers(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'GET':
        token=request.META['HTTP_AUTHORIZATION']
        if c.verifyToken(token=token):
            des = Atelier.readAll()
            data["error"] = False
            data["code"] = 0
            data["data"] = des
            status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
    return JsonResponse(data, status=status)

@csrf_exempt
def getAtelier(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'GET':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.getAtelier(payload)
        if c.verifyToken(token=token) and form.is_valid() :
            des = Atelier.read(
                uuid=form.cleaned_data["uuid"]
            )
            data["error"] = False
            data["code"] = 0
            data["data"] = des
            status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
    return JsonResponse(data, status=status)


