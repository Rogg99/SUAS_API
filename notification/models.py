from django.db import models
from .constances import constances as c
import datetime, uuid

# Create your models here.
class Notification(models.Model):
    uuid = models.CharField(max_length=100,primary_key=True)
    id_event = models.CharField(max_length=100,null=False)
    id_atelier = models.CharField(max_length=100,null=False)
    content = models.CharField(max_length=100, null=False)
    title = models.CharField(max_length=100, null=False)
    date_init = models.DateField(default=datetime.datetime.now())
    type = models.CharField(max_length=100,null=False,choices=c.NOTIFICATION_TYPE)

    class Meta:
        ordering =["date_init"]

    def __str__(self):
        return self.uuid

    def create(id_event: str, title: str, id_atelier: str, content: str):
        da = Notification()
        codefin = datetime.datetime.now().strftime("%Y-%m-%d")
        da.uuid = str(uuid.uuid4()) + ":"+str(len(Notification.objects.filter(date_init=codefin)))
        da.title = title
        da.id_atelier = id_atelier
        da.id_event = id_event
        da.content = content
        da.type = '1'
        da.save()
        return {
                    "uuid":da.uuid,
                    "id_event": da.id_event,
                    "id_atelier":da.id_atelier,
                    "title":da.title,
                    "type":da.type,
                    "content" : da.content,   
                    "date_init" : da.date_init,
                    }
    create = staticmethod(create)

    def update(uuid:str,id_event: str, type: str, title: str, id_atelier: str, content: str):
        da = Notification.objects.filter(uuid=uuid)
        da.title = title
        da.id_atelier = id_atelier
        da.id_event = id_event
        da.content = content
        da.type = type
        da.save()
        return da
    update = staticmethod(update)

    def read(uuid:str):
         item = Notification.objects.get(uuid=uuid)
         return {
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "id_atelier":item.id_atelier,
                    "title":item.title,
                    "type":item.type,
                    "content" : item.content,   
                    "date_init" : item.date_init,
                    }
    read = staticmethod(read)

    def readAll():
         events = Notification.objects.all()
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "id_atelier":item.id_atelier,
                    "title":item.title,
                    "type":item.type,
                    "content" : item.content,   
                    "date_init" : item.date_init,
                    })
         return res
    readAll = staticmethod(readAll)
