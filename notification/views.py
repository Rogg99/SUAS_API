from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Notification
from .constances import forms
from .constances import constances  as c
import json


@csrf_exempt
def createNotification(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'POST':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.InitNotification(payload)
        if c.verifyToken(token=token) and form.is_valid():
            title = forms.cleaned_data["title"]
            id_atelier = forms.cleaned_data["id_atelier"]
            id_event = forms.cleaned_data["id_event"]
            content = forms.cleaned_data["content"]
            try:
                des = Notification.create(
                    title=title,
                    id_atelier=id_atelier,
                    id_event=id_event,
                    content = content,
                )
                data["error"] = False
                data["code"] = 0
                data["data"] = des
                status = 200
            except Exception as e :
                data["error"] = True
                data["code"] = -1
                data['description'] = 'Database Writing error occured :' + e
                status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
        data['description'] = 'Unauthorized method GET'
    return JsonResponse(data, status=status)

@csrf_exempt
def updateNotification(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'POST':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.setNotification(payload)
        if c.verifyToken(token=token) and form.is_valid():
            uuid = form.cleaned_data["uuid"]
            title = forms.cleaned_data["title"]
            id_atelier = forms.cleaned_data["id_atelier"]
            id_event = forms.cleaned_data["id_event"]
            content = forms.cleaned_data["content"]
            _type = forms.cleaned_data["type"]
            try:
                des = Notification.update(
                    uuid=uuid,
                    title=title,
                    id_atelier=id_atelier,
                    id_event=id_event,
                    content = content,
                    type = _type,
                )
                data["error"] = False
                data["code"] = 0
                data["data"] = des
                status = 200
            except Exception as e :
                data["error"] = True
                data["code"] = -1
                data['description'] = 'Database Writing error occured :' + e
                status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
        data['description'] = 'Unauthorized method'
    return JsonResponse(data, status=status)

@csrf_exempt
def getNotifications(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'GET':
        token=request.META['HTTP_AUTHORIZATION']
        if c.verifyToken(token=token):
            des = Notification.readAll()
            data["error"] = False
            data["code"] = 0
            data["data"] = des
            status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
    return JsonResponse(data, status=status)

@csrf_exempt
def getNotification(request):
    data = {
        "error": True,
        "code": -4,
    }
    status = 400
    if request.method == 'GET':
        token=request.META['HTTP_AUTHORIZATION']
        payload = json.loads(request.body)
        form = forms.getNotification(payload)
        if c.verifyToken(token=token) and form.is_valid() :
            des = Notification.read(
                uuid=form.cleaned_data["uuid"]
            )
            data["error"] = False
            data["code"] = 0
            data["data"] = des
            status = 200
        else:
            status = 400
            data['code'] = -4
            data['error'] = True
            data['description'] = 'Bad Authorization'
    else:
        status = 405
        data['code'] = -5
        data['error'] = True
    return JsonResponse(data, status=status)


