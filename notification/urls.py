from django.urls import path
from . import views

urlpatterns = [
    path("Create", views.createNotification, name="createNotification"),
    path("Update", views.updateNotification, name="updateNotification"),
    path("Get", views.getNotification, name="getNotification"),
    path("GetAll", views.getNotifications, name="getNotifications"),
]