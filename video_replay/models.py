from django.db import models
from .constances import constances as c
import datetime, uuid

# Create your models here.
class Video(models.Model):
    uuid = models.CharField(max_length=100,primary_key=True)
    id_event = models.CharField(max_length=100,null=False)
    id_atelier = models.CharField(max_length=100,null=False)
    url = models.CharField(max_length=100,null=False)
    comment = models.CharField(max_length=100,null=False)                                                              
    date_init = models.DateField(auto_now_add=True)
    statut = models.CharField(max_length=100,null=False,choices=c.STATUT_VALIDATION_CHOICE)
    
    class Meta:
        ordering =["date_init"]

    def __str__(self):
        return self.uuid

    def create(id_event: str, url: str, id_atelier: str, comment: str):
        da = Video()
        codefin = datetime.datetime.now().strftime("%m/%Y")
        da.uuid = str(uuid.uuid4()) + ":"+str(len(Video.objects.filter(date_init=codefin)))
        da.url = url
        da.id_atelier = id_atelier
        da.id_event = id_event
        da.comment = comment
        da.statut = '2'
        da.save()
        return da
    create = staticmethod(create)

    def update(uuid:str,id_event: str,statut: str, url: str, id_atelier: str, comment: str):
        da = Video.objects.get(uuid=uuid)
        da.url = url
        da.id_atelier = id_atelier
        da.id_event = id_event
        da.comment = comment
        da.statut = statut
        da.save()
        return da
    update = staticmethod(update)

    def read(uuid:str):
         events = Video.objects.filter(uuid=uuid)
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "id_atelier":item.id_atelier,
                    "url":item.url,
                    "statut":item.statut,
                    "comment" : item.comment,   
                    "date_init" : item.date_init,
                    })
         return res
    read = staticmethod(read)

    def readAll(id_event:str):
         events = Video.objects.filter(id_event=id_event).exclude(statut='5')
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "id_atelier":item.id_atelier,
                    "url":item.url,
                    "statut":item.statut,
                    "comment" : item.comment,   
                    "date_init" : item.date_init,
                    })
         return res
    readAll = staticmethod(readAll)
