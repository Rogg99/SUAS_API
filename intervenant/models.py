from django.db import models
from .constances import constances as c
import datetime, uuid

# Create your models here.
class Intervenant(models.Model):
    uuid = models.CharField(max_length=100,primary_key=True)
    id_event = models.CharField(max_length=100,null=False)
    id_atelier = models.CharField(max_length=100,null=False)
    image = models.CharField(max_length=10000000,null=False)
    type_intervenant = models.CharField(max_length=100, choices=c.INTERVENANT_CHOICE)
    presentation = models.CharField(max_length=1000, null=False, blank=True, default='')  
    date_init = models.DateField(default=datetime.datetime.now())
    statut = models.CharField(max_length=100,null=False,choices=c.STATUT_VALIDATION_CHOICE)
    
    class Meta:
        ordering =["date_init"]

    def __str__(self):
        return self.uuid

    def create(id_event: str, image: str, id_atelier: str, type_intervenant: str, presentation: str):
        da = Intervenant()
        codefin = datetime.datetime.now().strftime("%Y-%m-%d")
        da.uuid = str(uuid.uuid4()) + ":"+str(len(Intervenant.objects.filter(date_init=codefin)))
        da.image = image
        da.id_atelier = id_atelier
        da.id_event = id_event
        da.type_intervenant = type_intervenant
        da.presentation = presentation
        da.statut = '1'
        da.save()
        return {
                    "uuid":da.uuid,
                    "id_event": da.id_event,
                    "id_atelier":da.id_atelier,
                    "type_intervenant":da.type_intervenant,
                    "image":da.image,
                    "statut":da.statut,
                    "presentation" : da.presentation,   
                    "date_init" : da.date_init,
                    }
    create = staticmethod(create)

    def update(uuid: str,id_event: str, image: str, id_atelier: str, type_intervenant: str, statut: str, presentation: str):
        da = Intervenant.objects.filter(uuid=uuid)
        da.image = image
        da.id_atelier = id_atelier
        da.id_event = id_event
        da.type_intervenant = type_intervenant
        da.presentation = presentation
        da.statut = statut
        da.save()
        return {
                    "uuid":da.uuid,
                    "id_event": da.id_event,
                    "id_atelier":da.id_atelier,
                    "type_intervenant":da.type_intervenant,
                    "image":da.image,
                    "statut":da.statut,
                    "presentation" : da.presentation,   
                    "date_init" : da.date_init,
                    }
    update = staticmethod(update)

    def read(uuid:str):
         item = Intervenant.objects.get(uuid=uuid)
         return {
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "id_atelier":item.id_atelier,
                    "type_intervenant":item.type_intervenant,
                    "image":item.image,
                    "statut":item.statut,
                    "presentation" : item.presentation,   
                    "date_init" : item.date_init,
                    }
    read = staticmethod(read)

    def readAll(id_event:str):
         events = Intervenant.objects.filter(id_event=id_event)
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "id_atelier":item.id_atelier,
                    "type_intervenant":item.type_intervenant,
                    "image":item.image,
                    "statut":item.statut,
                    "presentation" : item.presentation,   
                    "date_init" : item.date_init,
                    })
         return res
    readAll = staticmethod(readAll)
