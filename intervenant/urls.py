from django.urls import path
from . import views

urlpatterns = [
    path("Create", views.createIntervenant, name="createIntervenant"),
    path("Update", views.updateIntervenant, name="updateIntervenant"),
    path("Get", views.getIntervenant, name="getIntervenant"),
    path("GetAll", views.getIntervenants, name="getIntervenants"),
]