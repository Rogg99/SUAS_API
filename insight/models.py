from django.db import models
from .constances import constances as c
import datetime, uuid

# Create your models here.
class Insight(models.Model):
    uuid = models.CharField(max_length=100,primary_key=True)
    id_event = models.CharField(max_length=100,null=False)
    lienUrl = models.CharField(max_length=200,null=False)
    image = models.CharField(max_length=10000000,null=False)
    titre = models.CharField(max_length=100, null=False)                                                              
    date_init = models.DateField(auto_now_add=True)
    statut = models.CharField(max_length=100,null=False,choices=c.STATUT_VALIDATION_CHOICE)
    
    class Meta:
        ordering =["date_init"]

    def __str__(self):
        return self.uuid

    def create(id_event: str, image: str, lienUrl: str, titre: str):
        da = Insight()
        codefin = datetime.datetime.now().strftime("%m/%Y")
        da.uuid = str(uuid.uuid4()) + ":"+str(len(Insight.objects.filter(date_init=codefin)))
        da.image = image
        da.lienUrl = lienUrl
        da.id_event = id_event
        da.titre = titre
        da.statut = '1'
        da.save()
        return da
    create = staticmethod(create)
    
    def update(uuid: str,id_event: str, statut: str, image: str, lienUrl: str, titre: str):
        da = Insight.objects.get(uuid=uuid)
        da.image = image
        da.lienUrl = lienUrl
        da.id_event = id_event
        da.titre = titre
        da.statut = statut
        da.save()
        return da
    update = staticmethod(update)

    def read(uuid:str):
         events = Insight.objects.filter(uuid=uuid)
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "lienUrl":item.lienUrl,
                    "image":item.image,
                    "statut":item.statut,
                    "titre" : item.titre,   
                    "date_init" : item.date_init,
                    })
         return res
    read = staticmethod(read)

    def readAll(uuid:str):
         events = Insight.objects.exclude(statut=c.STATUT_VALIDATION_CHOICE[4])
         res = []
         for item in events:
            if item.uuid!="":
                res.append({
                    "uuid":item.uuid,
                    "id_event": item.id_event,
                    "lienUrl":item.lienUrl,
                    "image":item.image,
                    "statut":item.statut,
                    "titre" : item.titre,   
                    "date_init" : item.date_init,
                    })
         return res
    readAll = staticmethod(readAll)
